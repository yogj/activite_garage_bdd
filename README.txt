Bonjour

Vous trouverez dans ce dossier mon activit� 2 du cours JAVA sur le garage li� � une base de donn�e.

Pour r�pondre � la consigne : 
	- j'ai cr�e des objets DAO dans le packages garage_bdd_test_DAO avec une classe abstraite et ses diff�rentes classes filles. J'ai r�dig� dans ces objets les diff�rentes
 m�thodes executant les requetes n�cessaires
	
	- j'ai cr�e les boites de dialogues modales pour consulter le d�tails d'un v�hicule ou pour en cr�er un dans le package garage_bdd_test_boite_dialogue

	- j'ai utilis� un pattern Observer pour mettre � jour l'affichage de la table apr�s ajout/suppression d'un v�hicule. Les interfaces Observer et Observable se trouvent
 dans le package garage_bdd_patter_obs_affichage
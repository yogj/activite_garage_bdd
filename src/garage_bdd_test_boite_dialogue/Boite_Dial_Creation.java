package garage_bdd_test_boite_dialogue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.ocr.ihm.EmptyFieldException;
import garage_bdd_pattern_obs_affichage.Observateur;
import voiture.Marque;
import voiture.Vehicule;
import voiture.moteur.Moteur;
import voiture.moteur.TypeMoteur;
import voiture.option.Option;
/**
 * Boite de dialogue modale pour la creation d'un vehicule
 * @author nicolas
 *
 */
public class Boite_Dial_Creation extends Boite_dial_modale {
	private static final long serialVersionUID = 1L;

	
	/**
	 * Constructeur avec param
	 * @param parent
	 * @param ptitre
	 * @param modal
	 * @param pObs
	 */
	public Boite_Dial_Creation(JFrame parent, String ptitre, boolean modal, Observateur pObs)throws EmptyFieldException {
		super(parent, ptitre, modal, pObs);
	}
	
	/**
	 * Methode initialisant les diff�rents composants de la boite de dialogue
	 */
	public void initComponent() {
		titre = "CREATION DE VEHICULE";
		
		//--nom du vehicule
		JPanel panNom = new JPanel();
		panNom.setBackground(Color.WHITE);
		panNom.setPreferredSize(new Dimension (220,60));
		nom = new JTextField();
		nom.setPreferredSize(new Dimension(100,25));
		nom.setHorizontalAlignment(JLabel.RIGHT);
		
		panNom.setBorder(BorderFactory.createTitledBorder("Nom du v�hicule"));
		JLabel nomLabel = new JLabel("Saisir un nom : ");
		panNom.add(nomLabel);
		panNom.add(nom);
		
		//--Le panneau qui accueuille l'image
		JPanel panIcon = new JPanel();
		panIcon.setBackground(Color.WHITE);
		panIcon.setLayout(new BorderLayout());
				
		//--Marque
		JPanel panMarque = new JPanel();
		panMarque.setBackground(Color.WHITE);
		panMarque.setPreferredSize(new Dimension(220,60));
		panMarque.setBorder(BorderFactory.createTitledBorder("Marque du vehicule"));
		marque = new JComboBox<String>();
		marque.addItem("PIGEOT");
		marque.addItem("TROEN");
		marque.addItem("RENO");
		marque.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				panIcon.removeAll();
				//--Image du vehicule selon la marque
				String constrVehic = (String)marque.getSelectedItem();
				switch(constrVehic) {
					case("RENO") : panIcon.add(new JLabel(new ImageIcon("Ressources/Images/RENO.jpg"))); break;
					case("TROEN") : panIcon.add(new JLabel(new ImageIcon("Ressources/Images/TROEN.jpg"))) ; break;
					case ("PIGEOT") : panIcon.add(new JLabel(new ImageIcon("Ressources/Images/PIGEOT.jpg"))); break;
				}
				panIcon.revalidate();
				panIcon.repaint();
				tranche1.setEnabled(true);
				tranche2.setEnabled(true);
				tranche3.setEnabled(true);
				tranche4.setEnabled(true);
			}
		});

		JLabel marqueLabel = new JLabel("Marque : ");
		panMarque.add(marqueLabel);
		panMarque.add(marque);
						
		//--type de moteur
		JPanel panTypeMoteur = new JPanel();
		panTypeMoteur.setBackground(Color.WHITE);
		panTypeMoteur.setPreferredSize(new Dimension(440,60));
		panTypeMoteur.setBorder(BorderFactory.createTitledBorder("Type du moteur du vehicule"));
		tranche1.setEnabled(false);
		tranche1.setBackground(Color.WHITE);
		tranche1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cylindre.removeAllItems(); 
				cylindre.addItem("150 Chevaux");
				cylindre.addItem("200 Chevaux");	
				cylindre.setEnabled(true);
			}
		});
		
		tranche2.setEnabled(false);
		tranche2.setBackground(Color.WHITE);
		tranche2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cylindre.removeAllItems();;
				cylindre.addItem("150 Hdi");
				cylindre.addItem("180 Hdi");
				cylindre.addItem("250 Hdi");
				cylindre.setEnabled(true);
			}
		});
		
		tranche3.setEnabled(false);
		tranche3.setBackground(Color.WHITE);
		tranche3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cylindre.removeAllItems();
				cylindre.addItem("Essence/Nucleaire");
				cylindre.addItem("Essence/Eolienne");	
				cylindre.setEnabled(true);
			}			
		});
		
		tranche4.setEnabled(false);
		tranche4.setBackground(Color.WHITE);
		tranche4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cylindre.removeAllItems();
				cylindre.addItem("100 Kw");
				cylindre.addItem("1000 Kw");
				cylindre.setEnabled(true);
			}
		});
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(tranche1);
		bg.add(tranche2);
		bg.add(tranche3);
		bg.add(tranche4);
		panTypeMoteur.add(tranche1);
		panTypeMoteur.add(tranche2);
		panTypeMoteur.add(tranche3);
		panTypeMoteur.add(tranche4);
		this.typeMoteur = "";
		if(tranche1.isSelected())
			typeMoteur = tranche1.getText();
		else if (tranche2.isSelected())
			typeMoteur = tranche2.getText();
		else if (tranche3.isSelected())
			typeMoteur = tranche3.getText();
		else if (tranche4.isSelected())
			typeMoteur = tranche4.getText();
		
		//--cylindree du vehicule
		JPanel panCyl = new JPanel();
		panCyl.setBackground(Color.WHITE);
		panCyl.setPreferredSize(new Dimension(220,60));
		panCyl.setBorder(BorderFactory.createTitledBorder("Cylindree du vehicule"));
		JLabel cylindreLabel = new JLabel("Cylindree : ");
		cylindre = new JComboBox<String>();
		cylindre.addActionListener(new PrixListener());
		cylindre.setEnabled(false);
		cylindre.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				option1.setEnabled(true);
				option2.setEnabled(true);
				option3.setEnabled(true);
				option4.setEnabled(true);
				option5.setEnabled(true);				
			}
		});
		panCyl.add(cylindreLabel);
		panCyl.add(cylindre);
		
		//--Options
		JPanel panOption = new JPanel();
		panOption.setBackground(Color.WHITE);
		panOption.setPreferredSize(new Dimension(700,60));
		panOption.setBorder(BorderFactory.createTitledBorder("Options du vehicule"));
		
		option1 = new JCheckBox("Toit ouvrant");
		option1.addActionListener(new PrixListener());
		option1.setBackground(Color.WHITE);
		option1.setEnabled(false);
		
		option2 = new JCheckBox("Climatisation");
		option2.addActionListener(new PrixListener());
		option2.setBackground(Color.WHITE);
		option2.setEnabled(false);
		
		option3 = new JCheckBox("GPS");
		option3.addActionListener(new PrixListener());
		option3.setBackground(Color.WHITE);
		option3.setEnabled(false);
		
		option4 = new JCheckBox("Si�ges chauffants");
		option4.addActionListener(new PrixListener());
		option4.setBackground(Color.WHITE);
		option4.setEnabled(false);
		
		option5 = new JCheckBox("Barres de toit");
		option5.addActionListener(new PrixListener());
		option5.setBackground(Color.WHITE);
		option5.setEnabled(false);
		
		panOption.add(option1);
		panOption.add(option2);
		panOption.add(option3);
		panOption.add(option4);
		panOption.add(option5);
		
		//--Prix du vehicule
		JPanel panPrix = new JPanel();
		panPrix.setBackground(Color.WHITE);
		panPrix.setPreferredSize(new Dimension (220,60));
		panPrix.setBorder(BorderFactory.createTitledBorder("Prix du vehicule"));
		JLabel prixLabel1 = new JLabel("Prix");
		JLabel prixLabel2 = new JLabel(" �");
		prix = new JFormattedTextField(NumberFormat.getNumberInstance());
		prix.setPreferredSize(new Dimension (90,25));
		prix.setHorizontalAlignment(JLabel.RIGHT);
		prix.setText(String.valueOf(prixTot));
		prix.addActionListener(new PrixListener());
		prix.setEnabled(false);
		
		panPrix.add(prixLabel1);
		panPrix.add(prix);
		panPrix.add(prixLabel2);
				
		//--On ajoute les composants precedents au JPanel qui doit recevoir tout cela		
		panContent.add(panNom);
		panContent.add(panMarque);
		panContent.add(panTypeMoteur);
		panContent.add(panCyl);
		panContent.add(panOption);
		panContent.add(panPrix);
		panContent.add(panIcon);
		
		//--On ajoute un listener a notre bouton OK
		this.okBouton.addActionListener(new BoutonListener());

	}
	/**
	 * classe interne qui determine le comportement du bouton OK
	 * @author nicolas
	 *
	 */
	class BoutonListener implements ActionListener {
		
		/**
		 * Methode qui utilise les donnees entrees par l'utilisateur pour creer un vehicule dans la bdd
		 */
		public void actionPerformed(ActionEvent e) {
			
			Vehicule vehiculeCree = new Vehicule();
			//--On recupere la marque
			Marque marqueVehicCree = new Marque();
			String nameMarque = (String)marque.getSelectedItem().toString();
			int idMarque = marqueDAO.getIdentifiant(nameMarque).getId();
			marqueVehicCree.setNom(nameMarque);
			marqueVehicCree.setId(idMarque);
			
			//--on construit notre vehicule
			vehiculeCree.setNom(nom.getText());
			vehiculeCree.setMarque(marqueVehicCree);
			vehiculeCree.setMoteur(getMoteur());
			vehiculeCree.setListOptions(getOption());
			vehiculeCree.setPrix(getPrix());
			vehDAO.create(vehiculeCree);
			updateObservateur();
			setVisible(false);
			System.out.println("boite dial : "+vehiculeCree.toString());//--Controle	
		}
		
		/**
		 * Methode qui renvoie l'objet moteur du vehicule cree
		 * @return Moteur
		 */
		public Moteur getMoteur() {
			Moteur moteurCree = motDAO.getIdentifiant(cylindre.getSelectedItem().toString());
			return moteurCree;
		}
		
		/**
		 * Methode qui renvoie le type de moteur du vehicule cree. Inutile
		 * @return
		 */
		public TypeMoteur getTypeMoteur() {
			TypeMoteur typeMotCree = tmDAO.getIdentifiant(typeMoteur);
			return typeMotCree ;
		}
		
		/**
		 * Methode qui renvoie la liste des options du vehicule cree
		 * @return
		 */
		public ArrayList<Option> getOption(){
			ArrayList<Option> optionCree = new ArrayList<Option>();
			if(option1.isSelected()) {
				Option opt1 = (optDAO.getIdentifiant(option1.getText()));
				optionCree.add(opt1);
			}
			if(option2.isSelected()) {
				Option opt2 = optDAO.getIdentifiant(option2.getText());
				optionCree.add(opt2);
			}	
			if(option3.isSelected()) {
				Option opt3 = optDAO.getIdentifiant(option3.getText());
				optionCree.add(opt3);
			}
			if(option4.isSelected()) {
				Option opt4 = optDAO.getIdentifiant(option4.getText());
				optionCree.add(opt4);
			}
			if(option5.isSelected()) {
				Option opt5 = optDAO.getIdentifiant(option5.getText());
				optionCree.add(opt5);
			}
			return optionCree;
		}
		/**
		 * Methode qui renvoie le prix du v�hicule
		 * @return
		 */
		public double getPrix() {
			double prixTotal = 0d;
			
			double prixMotCree = getMoteur().getPrix();
			System.out.println("boite_dial_creation prix motCree: "+prixMotCree);//--Controle
			double prixOptCree = 0;
			for (Option opt : this.getOption())
				prixOptCree += opt.getPrix();
			System.out.println("boite_dial_creation prix optCree: "+prixOptCree);//--Controle
			
			prixTotal = (((Number) prix.getValue()).doubleValue() - prixOptCree);
			return prixTotal;			
		}
	}
	/**
	 * classe interne qui assure la mise � jour du prix dans l'�tiquette prix
	 * @author nicolas
	 *
	 */
	class PrixListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			//--On recupere le prix du moteur et on l'ajoute au prix total
			double prixMotCree = 0d;
			if(cylindre.isEnabled()) {
				prixMotCree = motDAO.getIdentifiant(cylindre.getSelectedItem().toString()).getPrix();
				System.out.println("boite_dial_creation - cylindre = \""+cylindre.getSelectedItem().toString()+"\"");//--Controle
				System.out.println("boite_dial_creation prix motCree: "+prixMotCree);//--Controle
				prix.setText(String.valueOf(prixMotCree));
			}
			
			//--On ajoute le prix des options aux prix total en le recuperant dans la bdd
			double prixOptCree = 0d;
			if (option1.isSelected()) { 
				prixOptCree += optDAO.getIdentifiant(option1.getText()).getPrix();
				prix.setEnabled(true);
				prix.setEditable(true);
			}
			if (option2.isSelected()) {
				prixOptCree += optDAO.getIdentifiant(option2.getText()).getPrix();
				prix.setEnabled(true);
				prix.setEditable(true);
			}
			if (option3.isSelected()) {
				prixOptCree += optDAO.getIdentifiant(option3.getText()).getPrix();
				prix.setEnabled(true);
				prix.setEditable(true);
			}
			if (option4.isSelected()) {
				prixOptCree += optDAO.getIdentifiant(option4.getText()).getPrix();
				prix.setEnabled(true);
				prix.setEditable(true);
			}
			if (option5.isSelected()) {
				prixOptCree += optDAO.getIdentifiant(option5.getText()).getPrix();
				prix.setEnabled(true);
				prix.setEditable(true);
			}
			System.out.println("boite_dial_creation prix optCree: "+prixOptCree);//--Controle
			prix.setText(String.valueOf(prixMotCree + prixOptCree));
			
			//--On r�cup�re le prix de l'utilisateur
			if (prix.isEnabled()) {
				
				try {
					prixTot += Double.parseDouble(prix.getText());
					//prixTot += ((Number) prix.getValue()).doubleValue();
				
				}catch (NullPointerException err) {
					System.out.println("prix_tot_boite_crea"+ prixTot);
					err.printStackTrace();
				}
			}
			
			//--On met � jour l'affichage du prix
			prix.setText(String.valueOf(prixTot + prixMotCree + prixOptCree));			
		}
		
	}
}

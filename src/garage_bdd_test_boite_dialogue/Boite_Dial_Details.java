package garage_bdd_test_boite_dialogue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.ocr.sql.HsqldbConnection;
import garage_bdd_test_DAO.DAO_abstr;
import garage_bdd_test_DAO.VehiculeDAO;
import voiture.Vehicule;
import voiture.option.Option;


/**
 * Boite de dialogue modale pour afficher les details d'un vehicule
 * @author nicolas
 *
 */
public class Boite_Dial_Details extends Boite_dial_modale {
	private static final long serialVersionUID = 1L;
	private Vehicule vehicDetails;

	public Boite_Dial_Details(JFrame parent, String ptitre, boolean modal, int pID) {
		super(parent, ptitre, modal, pID);
	}
	
	/**
	 * Methode initialisant les diff�rents composants de la boite de dialogue
	 */
	public void initComponent() {
		titre = "DETAILS DU VEHICULE";
		DAO_abstr<Vehicule> vehDAO = new VehiculeDAO(HsqldbConnection.getInstance());
		this.vehicDetails = vehDAO.find(id);//--recup l'id du vehicule � partir de la table
		System.out.println("details du vehicule : "+id+" - " +vehicDetails.toString());//--Controle
			
		//--nom du vehicule
		JPanel panNom = new JPanel();
		panNom.setBackground(Color.WHITE);
		panNom.setPreferredSize(new Dimension (220,60));
		panNom.setBorder(BorderFactory.createTitledBorder("Nom du v�hicule"));
		JLabel nomLabel = new JLabel(vehicDetails.getNom());
		panNom.add(nomLabel);
		
		//--Marque
		JPanel panMarque = new JPanel();
		panMarque.setBackground(Color.WHITE);
		panMarque.setPreferredSize(new Dimension(220,60));
		panMarque.setBorder(BorderFactory.createTitledBorder("Marque du vehicule"));
		JLabel marqueLabel = new JLabel("Marque : ");
		JLabel marque = new JLabel(vehicDetails.getMarque().getNom());
		panMarque.add(marqueLabel);
		panMarque.add(marque);
		
		//--Image du vehicule selon la marque
		String constrVehic = marque.getText();
		switch(constrVehic) {
			case("RENO") : this.icon = new JLabel(new ImageIcon("Ressources/Images/RENO.jpg")); break;
			case("TROEN") : this.icon = new JLabel(new ImageIcon("Ressources/Images/TROEN.jpg")) ; break;
			case ("PIGEOT") : this.icon = new JLabel(new ImageIcon("Ressources/Images/PIGEOT.jpg")); break;
		}
		
		//--Le panneau qui accueuille l'image
		JPanel panIcon = new JPanel();
		panIcon.setBackground(Color.WHITE);
		panIcon.setLayout(new BorderLayout());
		panIcon.add(icon);
				
		//--type de moteur
		JPanel panTypeMoteur = new JPanel();
		panTypeMoteur.setBackground(Color.WHITE);
		panTypeMoteur.setPreferredSize(new Dimension(440,60));
		panTypeMoteur.setBorder(BorderFactory.createTitledBorder("Type du moteur du vehicule"));
		tranche1.setEnabled(false);
		tranche1.setBackground(Color.WHITE);
		tranche2.setEnabled(false);
		tranche2.setBackground(Color.WHITE);
		tranche3.setEnabled(false);
		tranche3.setBackground(Color.WHITE);
		tranche4.setEnabled(false);
		tranche4.setBackground(Color.WHITE);
		String constrTM = vehicDetails.getMoteur().getType().getNom();
		switch(constrTM) {
			case("ESSENCE") : tranche1.setSelected(true); break;
			case("DIESEL") : tranche2.setSelected(true) ; break;
			case ("HYBRIDE") : tranche3.setSelected(true); break;
			case("ELECTRIQUE") : tranche4.setSelected(true); break;
		}
		ButtonGroup bg = new ButtonGroup();
		bg.add(tranche1);
		bg.add(tranche2);
		bg.add(tranche3);
		bg.add(tranche4);
		panTypeMoteur.add(tranche1);
		panTypeMoteur.add(tranche2);
		panTypeMoteur.add(tranche3);
		panTypeMoteur.add(tranche4);
		
		//--cylindree du vehicule
		JPanel panCyl = new JPanel();
		panCyl.setBackground(Color.WHITE);
		panCyl.setPreferredSize(new Dimension(220,60));
		panCyl.setBorder(BorderFactory.createTitledBorder("Cylindree du vehicule"));
		JLabel cylindreLabel = new JLabel("Cylindree : ");
		JLabel cylDetail = new JLabel(vehicDetails.getMoteur().getCylindre());
		panCyl.add(cylindreLabel);
		panCyl.add(cylDetail);
		
		//--Options
		JPanel panOption = new JPanel();
		panOption.setBackground(Color.WHITE);
		panOption.setPreferredSize(new Dimension(700,60));
		panOption.setBorder(BorderFactory.createTitledBorder("Options du vehicule"));
		option1 = new JCheckBox("Toit ouvrant");
		option1.setEnabled(false);
		option1.setBackground(Color.WHITE);
		option2 = new JCheckBox("Climatisation");
		option2.setEnabled(false);
		option2.setBackground(Color.WHITE);
		option3 = new JCheckBox("GPS");
		option3.setEnabled(false);
		option3.setBackground(Color.WHITE);
		option4 = new JCheckBox("Si�ges chauffants");
		option4.setEnabled(false);
		option4.setBackground(Color.WHITE);
		option5 = new JCheckBox("Barres de toit");
		option5.setEnabled(false);
		option5.setBackground(Color.WHITE);
		ArrayList<Option> constrOpt = vehicDetails.getOptions();
		
		for (Option opt : constrOpt) {
			System.out.println("Boite_dial_detail option : "+opt.toString());//--Controle
			if (opt.getNom().equals("Toit ouvrant"))
				option1.setSelected(true);
			if (opt.getNom().equals("Climatisation"))
				option2.setSelected(true);
			if(opt.getNom().equals("GPS"))
				option3.setSelected(true);
			if(opt.getNom().equals("Si�ges chauffants"))
				option4.setSelected(true);
			if (opt.getNom().equals("Barres de toit"))
				option5.setSelected(true);			
		}
		panOption.add(option1);
		panOption.add(option2);
		panOption.add(option3);
		panOption.add(option4);
		panOption.add(option5);
		
		//--Prix du vehicule
		JPanel panPrix = new JPanel();
		panPrix.setBackground(Color.WHITE);
		panPrix.setPreferredSize(new Dimension (220,60));
		panPrix.setBorder(BorderFactory.createTitledBorder("Prix du vehicule"));
		JLabel prixLabel1 = new JLabel("Prix");
		JLabel prixLabel2 = new JLabel(" �");
		JLabel prix = new JLabel(vehicDetails.getPrixTotal().toString());
		panPrix.add(prixLabel1);
		panPrix.add(prix);
		panPrix.add(prixLabel2);
				
		//--On ajoute les composants precedents au JPanel qui doit recevoir tout cela		
		panContent.add(panNom);
		panContent.add(panMarque);
		panContent.add(panTypeMoteur);
		panContent.add(panCyl);
		panContent.add(panOption);
		panContent.add(panPrix);
		panContent.add(panIcon);
		
		//--On ajoute un listener a notre bouton OK
		this.okBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
	}
}

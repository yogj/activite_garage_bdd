package garage_bdd_test_boite_dialogue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import fr.ocr.ihm.EmptyFieldException;
import fr.ocr.sql.HsqldbConnection;
import garage_bdd_pattern_obs_affichage.Observable;
import garage_bdd_pattern_obs_affichage.Observateur;
import garage_bdd_test_DAO.DAO_abstr;
import garage_bdd_test_DAO.MarqueDAO;
import garage_bdd_test_DAO.MoteurDAO;
import garage_bdd_test_DAO.OptionDAO;
import garage_bdd_test_DAO.TypeMoteurDAO;
import garage_bdd_test_DAO.VehiculeDAO;
import voiture.Marque;
import voiture.Vehicule;
import voiture.moteur.Moteur;
import voiture.moteur.TypeMoteur;
import voiture.option.Option;
/**
 * Classe abstraite boite de dialogue modale dont herite les 3 boites de dialogue : creation, detail, suppression
 * @author nicolas
 *
 */
public abstract class Boite_dial_modale extends JDialog implements Observable {
	private static final long serialVersionUID = 1L;
	protected JFrame frame;
	protected Connection conn = HsqldbConnection.getInstance();
	protected JPanel panContent = new JPanel();
	protected JLabel icon = new JLabel(new ImageIcon());
	protected JButton okBouton = new JButton("OK");
	protected JTextField nom;
	protected JComboBox<String> marque, cylindre;
	protected JFormattedTextField prix;
	protected JCheckBox option1, option2, option3, option4, option5;
	protected String typeMoteur, titre;
	protected JRadioButton tranche1 = new JRadioButton("ESSENCE");
	protected JRadioButton tranche2 = new JRadioButton("DIESEL");
	protected JRadioButton tranche3 = new JRadioButton("HYBRIDE");
	protected JRadioButton tranche4 = new JRadioButton("ELECTRIQUE");
	protected double prixTot = 0d;
	protected int id;
	protected DAO_abstr<Vehicule> vehDAO = new VehiculeDAO(HsqldbConnection.getInstance());
	protected DAO_abstr<Marque> marqueDAO = new MarqueDAO(HsqldbConnection.getInstance());
	protected DAO_abstr<Moteur> motDAO = new MoteurDAO(HsqldbConnection.getInstance());
	protected DAO_abstr<TypeMoteur> tmDAO = new TypeMoteurDAO(HsqldbConnection.getInstance());
	protected DAO_abstr<Option> optDAO = new OptionDAO(HsqldbConnection.getInstance());
	protected ArrayList<Observateur> listObs =  new ArrayList<Observateur>();
	
	/**
	 * Constructeur avec parametre pour la suppression et le detail
	 * @param parent
	 * @param ptitre
	 * @param modal
	 * @param id du vehicule
	 */
	public Boite_dial_modale (JFrame parent, String ptitre, boolean modal, int pID){
		super(parent, ptitre, modal);
		this.id = pID;
		this.setSize(1000,400);
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setUndecorated(true);
		
		//--On appel la methode qui initialise les diff�rents panneaux de donnees
		this.initComponent();
		//--Le panneau qui recueille les composants de la methodes precedente.Vide pour l'instant
		panContent.setLayout(new FlowLayout());
		panContent.setBackground(Color.WHITE);
		panContent.setBorder(BorderFactory.createMatteBorder(0,	 1, 0, 1, Color.BLACK));
		
		
		//--Le panneau qui recueille les boutons
		JPanel panBouton = new JPanel();
		panBouton.setLayout(new FlowLayout());
		panBouton.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		
		//--Le bouton OK est juste creer, son comportement est def ds les classes filles

		//--Bouton ANNULER
		JButton annulBouton = new JButton ("ANNULER");
		annulBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		panBouton.add(okBouton);
		panBouton.add(annulBouton);
		
		//--Une etiquette de titre
		JLabel titreLbl = new JLabel(titre, JLabel.CENTER);
		Font policetitre = new Font("Arial", Font.BOLD, 25);
		titreLbl.setFont(policetitre);
		titreLbl.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		
		this.getContentPane().add(titreLbl, BorderLayout.NORTH);
		this.getContentPane().add(panContent, BorderLayout.CENTER);
		this.getContentPane().add(panBouton, BorderLayout.SOUTH);
		
		this.setVisible(true);
	}
	
	/**
	 * Constructeur avec parametre pour la creation
	 * @param parent
	 * @param ptitre
	 * @param modal
	 */
	public Boite_dial_modale (JFrame parent, String ptitre, boolean modal, Observateur pObs)throws EmptyFieldException {
		super(parent, ptitre, modal);
		this.setSize(1000, 400);
		this.setLocationRelativeTo(null);
		this.setUndecorated(true);
		
		//--On met un observateur pour la mise � jour de l'affichage de la fenetre
		this.addObservateur(pObs);
		
		//--On appel la methode qui initialise les diff�rents panneaux de donnees
		this.initComponent();
		
		//--Le panneau qui recueille les composants de la methodes precedente.Vide pour l'instant
		panContent.setLayout(new FlowLayout());
		panContent.setBackground(Color.WHITE);
		panContent.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 1, Color.BLACK));
		
		//--Le panneau qui recueille les boutons
		JPanel panBouton = new JPanel();
		panBouton.setLayout(new FlowLayout());
		panBouton.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		
		//--Le bouton OK est juste creer, son comportement est def ds les classes filles
		
		//--Bouton ANNULER
		JButton annulBouton = new JButton ("ANNULER");
		annulBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		panBouton.add(okBouton);
		panBouton.add(annulBouton);
		
		//--Le panneau qui accueuille l'image
		JPanel panIcon = new JPanel();
		panIcon.setBackground(Color.WHITE);
		panIcon.setLayout(new BorderLayout());
		panIcon.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 1, Color.BLACK));
		panIcon.add(icon);
		
		//--Une etiquette de titre
		JLabel titreLbl = new JLabel(titre, JLabel.CENTER);
		Font policetitre = new Font("Arial", Font.BOLD, 25);
		titreLbl.setFont(policetitre);
		titreLbl.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		
		this.getContentPane().add(titreLbl, BorderLayout.NORTH);
		this.getContentPane().add(panContent, BorderLayout.CENTER);
		this.getContentPane().add(panBouton, BorderLayout.SOUTH);
		this.getContentPane().add(panIcon, BorderLayout.EAST);
		
		this.setVisible(true);
	}
	
	public void initComponent() {}
	
	/**
	 * Les 3 m�thodes du pattern Observable/Observateur
	 */
	@Override
	public void addObservateur(Observateur o) {
		this.listObs.add(o);
		
	}

	@Override
	public void updateObservateur() {
		for(Observateur o : listObs)
			o.update("Refresh");
	}

	@Override
	public void delObservateur() {
		this.listObs = new ArrayList<Observateur>();
		
	}
	
}

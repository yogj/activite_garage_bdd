package test_unitaire;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


import garage_bdd_test_boite_dialogue.Boite_Dial_Details;

/**
 * Classe de test de la boite de dialogue modale pour les details du vehicule
 * @author nicolas
 *
 */

public class Boite_Dial_Detail_Test extends JFrame {
	private static final long serialVersionUID = 1L;
	private JButton bouton = new JButton("Appel � la ZDialog");
	
	public Boite_Dial_Detail_Test() {
		this.setTitle("Boite de dialogue perso");
		this.setSize(300,100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(bouton);
		bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Boite_Dial_Details bdDetails = new Boite_Dial_Details(null, "DETAILS DU VEHICULE", true, 18);
			}
		});
		this.setVisible(true);
	}
		
	public static void main(String[] args) {
		Boite_Dial_Detail_Test f = new Boite_Dial_Detail_Test();

	}

}

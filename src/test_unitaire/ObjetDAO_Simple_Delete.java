package test_unitaire;

import fr.ocr.sql.HsqldbConnection;
import garage_bdd_test_DAO.DAO_abstr;
import garage_bdd_test_DAO.MarqueDAO;
import garage_bdd_test_DAO.OptionDAO;
import voiture.Marque;
import voiture.option.Option;

public class ObjetDAO_Simple_Delete {

	public static void main(String[] args) {

		Marque ford = new Marque();
		DAO_abstr<Marque> marqueDAO = new MarqueDAO(HsqldbConnection.getInstance());
		ford = marqueDAO.getIdentifiant("mercos");
		System.out.println("marque trouvee : "+ford.getId()+" - "+ford.getNom());
		marqueDAO.delete(ford);
		System.out.println("marque suppr : "+ford.getId()+" - "+ford.getNom());
		
		Option opt = new Option();
		DAO_abstr<Option> optDAO = new OptionDAO(HsqldbConnection.getInstance());
		opt = optDAO.getIdentifiant("siege bebe");
		System.out.println("option trouvee : "+opt.getId()+" - "+opt.getNom());
		optDAO.delete(opt);
		System.out.println("option suppr : "+opt.getId()+" - "+opt.getNom());
		
		HsqldbConnection.closeConnexion();
	//Connection connect = HsqldbConnection.getInstance();
	//try {
	//	connect.close();
	//}catch (SQLException e) {
	//	e.printStackTrace();
	//}

	}

}

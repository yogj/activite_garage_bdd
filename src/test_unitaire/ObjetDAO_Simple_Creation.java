package test_unitaire;

import java.sql.Connection;
import java.sql.SQLException;

import fr.ocr.sql.HsqldbConnection;
import garage_bdd_test_DAO.DAO_abstr;
import garage_bdd_test_DAO.MarqueDAO;
import garage_bdd_test_DAO.OptionDAO;
import voiture.Marque;
import voiture.option.Option;



public class ObjetDAO_Simple_Creation {
	
	
	public static void main(String[] args) {
		DAO_abstr<Marque> marqueDAO1 = new MarqueDAO(HsqldbConnection.getInstance());

		Marque ford = new Marque(-1,"hyundai");
		marqueDAO1.create(ford);
		Marque mcree = marqueDAO1.getIdentifiant(ford.getNom());
		System.out.println("marque cr�ee : "+(mcree.getNom()));
		
		DAO_abstr<Option> optDAO = new OptionDAO(HsqldbConnection.getInstance());
		Option siegeBB = new Option(-1,"siege bebe", 150d);
		optDAO.create(siegeBB);
		Option ocree = optDAO.getIdentifiant(siegeBB.getNom());
		System.out.println("option cr�ee : "+(ocree.getNom()));
		
		Connection connect = HsqldbConnection.getInstance();
		try {
			connect.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		//essayez la methode cree ds HsqldbConnection
	}

}

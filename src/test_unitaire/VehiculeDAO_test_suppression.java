package test_unitaire;

import java.sql.Connection;
import java.sql.SQLException;

import fr.ocr.sql.HsqldbConnection;
import garage_bdd_test_DAO.DAO_abstr;
import garage_bdd_test_DAO.VehiculeDAO;
import voiture.Vehicule;

public class VehiculeDAO_test_suppression {

	public static void main(String[] args) {
		DAO_abstr<Vehicule> vehicDAO = new VehiculeDAO(HsqldbConnection.getInstance());
		
		Vehicule veh = vehicDAO.find(112);
		System.out.println("voiture suppr : "+veh.toString());
		vehicDAO.delete(veh);
		
		Connection connect = HsqldbConnection.getInstance();
		try {
			connect.close();
		}catch (SQLException e) {
			e.printStackTrace();

		}

	}
}

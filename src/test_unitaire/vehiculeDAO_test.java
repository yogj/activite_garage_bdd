package test_unitaire;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import fr.ocr.sql.HsqldbConnection;
import garage_bdd_test_DAO.DAO_abstr;
import garage_bdd_test_DAO.MarqueDAO;
import garage_bdd_test_DAO.MoteurDAO;
import garage_bdd_test_DAO.OptionDAO;
import garage_bdd_test_DAO.VehiculeDAO;
import voiture.Marque;
import voiture.Vehicule;
import voiture.moteur.Moteur;
import voiture.option.Option;

public class vehiculeDAO_test {

	public static void main(String[] args) {
		Vehicule veh = new Vehicule();
		veh.setNom("ma F21");
		
		DAO_abstr<Marque> marqueDAO = new MarqueDAO(HsqldbConnection.getInstance());
		Marque m = marqueDAO.getIdentifiant("TROEN");
		veh.setMarque(m);
		
		DAO_abstr<Moteur> motDAO = new MoteurDAO(HsqldbConnection.getInstance());
		Moteur mot = motDAO.getIdentifiant("150 cv");
		veh.setMoteur(mot);
		
		DAO_abstr<Option> optDAO = new OptionDAO(HsqldbConnection.getInstance());
		ArrayList<Option> lopt = new ArrayList<Option>();
		Option opt = optDAO.getIdentifiant("Toit ouvrant");
		lopt.add(opt);
		veh.setListOptions(lopt);
		
		
		veh.setPrix(1500d);
		
		DAO_abstr<Vehicule> vehicDAO = new VehiculeDAO(HsqldbConnection.getInstance());
		vehicDAO.create(veh);
		System.out.println("creation du vehicule : "+veh.getMarque().getId()+" - "+veh.getMoteur().getId()+" - "+veh.getPrix()+" - "+veh.getNom()+" - "+veh.getId());
		
		Vehicule vehcree = vehicDAO.getIdentifiant(veh.getNom());
		System.out.println("vehic cree : "+vehcree.toString());
		
	//veh.setPrix(3000d);
	//vehicDAO.update(veh);
		
		Connection connect = HsqldbConnection.getInstance();
		try {
			connect.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		

	}

}

package test_unitaire;

import java.sql.Connection;
import java.sql.SQLException;

import fr.ocr.sql.HsqldbConnection;
import garage_bdd_test_DAO.DAO_abstr;
import garage_bdd_test_DAO.MarqueDAO;
import voiture.Marque;

public class ObjetDAO_Simple_Update {

	public static void main(String[] args) {
		DAO_abstr<Marque> marqueDAO = new MarqueDAO(HsqldbConnection.getInstance());

		Marque mercos = marqueDAO.getIdentifiant("mercos");
		System.out.println("marque trouvee : "+mercos.getId()+" - "+mercos.getNom());
		mercos.setNom("mercedes");
		mercos.setId(mercos.getId());
		marqueDAO.update(mercos);
		System.out.println("marque modif : "+mercos.getId()+" - "+mercos.getNom());
		
		Connection connect = HsqldbConnection.getInstance();
		try {
			connect.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		//essayez la methode cree ds HsqldbConnection

	}

}

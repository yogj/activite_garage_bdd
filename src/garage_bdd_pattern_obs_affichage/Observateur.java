package garage_bdd_pattern_obs_affichage;



/**
 * Interface Observateur du pattern Observer
 * avec la methode qui met a jour la table
 * @author nicolas
 *
 */
public interface Observateur {
	public void update(String value);
}

package fr.ocr.ihm.listener;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import fr.ocr.sql.HsqldbConnection;
import garage_bdd_pattern_obs_affichage.Observable;
import garage_bdd_pattern_obs_affichage.Observateur;
import garage_bdd_test_DAO.DAO_abstr;
import garage_bdd_test_DAO.VehiculeDAO;
import voiture.Vehicule;

//Notre listener pour le bouton
public class ButtonListener implements ActionListener, Observable {
	protected int column, row, id;
	protected JTable table;
	private ArrayList<Observateur> listObs = new ArrayList<Observateur>();
	private Observateur obs;
	
	
	public void setColumn(int col) {
		this.column = col;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public void setTable(JTable table) {
		this.table = table;
	}
	/**
	 * La table dans la quelle se trouve le bouton
	 * @param table
	 */
	public void setObservateur(Observateur obs) {
		this.obs = obs;
		this.addObservateur(obs);
	}

	public void actionPerformed(ActionEvent event) {
		this.id = Integer.valueOf((String)table.getValueAt(row, this.column-2));
		DAO_abstr<Vehicule> vehDAO = new VehiculeDAO(HsqldbConnection.getInstance());
		
		//--on controle l'objet Vehicule en le recuperant ds la bdd 
		Vehicule vehSuppr = vehDAO.find(id);
		System.out.println("vehSuppr bouton : "+vehSuppr.toString());//--Controle
		//Une boite de dialogue 
		JOptionPane jop = new JOptionPane();
		int option = jop.showConfirmDialog(null, "Voulez-vous supprimer un v�hicule ? ", "Suppression de v�hicule", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (option == JOptionPane.OK_OPTION) {
			//--on supprime de la bdd
			vehDAO.delete(vehSuppr);
			updateObservateur();			
		}
	}

	/**
	 * Les 3 m�thodes de l'interface Observable du pattern Observable/Observateur
	 */
	@Override
	public void addObservateur(Observateur o) {
		this.listObs.add(o);
		
	}

	@Override
	public void updateObservateur() {
		for (Observateur o : listObs) {
			o.update("refresh");
			}
	}

	@Override
	public void delObservateur() {
		this.listObs = new ArrayList<Observateur>();
		
	}
		
}
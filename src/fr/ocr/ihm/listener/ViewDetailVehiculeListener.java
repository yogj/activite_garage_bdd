package fr.ocr.ihm.listener;

import java.awt.event.ActionEvent;

import garage_bdd_test_boite_dialogue.Boite_Dial_Details;

public class ViewDetailVehiculeListener extends ButtonListener {
	private Integer id ;

	public void actionPerformed(ActionEvent e) {
		this.id =  Integer.valueOf((String)table.getValueAt(row, this.column-1));
		//Appel a boite de dialogue perso
		Boite_Dial_Details bdDetails = new Boite_Dial_Details(null, "DETAILS DU VEHICULE", true, id);
		
	}
}

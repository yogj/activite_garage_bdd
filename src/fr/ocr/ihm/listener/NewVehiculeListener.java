package fr.ocr.ihm.listener;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import fr.ocr.ihm.EmptyFieldException;
import fr.ocr.sql.DAOTableFactory;
import fr.ocr.sql.DatabaseTable;
import fr.ocr.sql.HsqldbConnection;
import garage_bdd_pattern_obs_affichage.Observateur;
import garage_bdd_test_boite_dialogue.Boite_Dial_Creation;

public class NewVehiculeListener implements ActionListener {

	private Observateur observateur;

	public NewVehiculeListener(Observateur pObs) {
		this.observateur = pObs;
	}

	public void actionPerformed(ActionEvent e) {
		//Appel a boite de dialogue perso
		try {
			Boite_Dial_Creation bdCreation = new Boite_Dial_Creation(null, "AJOUT DE VEHICULE", true, observateur);
		} catch (EmptyFieldException e1) {
			e1.printStackTrace();
		}
	}
}

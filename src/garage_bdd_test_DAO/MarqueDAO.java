package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.ocr.sql.HsqldbConnection;
import voiture.Marque;
import voiture.moteur.TypeMoteur;
/**
 * Classe d�finissant l'objet MarqueDAO
 * @author nicolas
 *
 */
public class MarqueDAO extends DAO_abstr<Marque> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public MarqueDAO(Connection pConn) {
		super(pConn);
	}

	/**
	 * M�thode de cr�ation d'un objet Marque dans la table Marque
	 */
	public boolean create(Marque obj) {
		DAO_abstr<Marque> marqueDAO = new MarqueDAO(HsqldbConnection.getInstance());
		try {
			this.conn.setAutoCommit(false);
			//On verifie que l'objet n'existe pas deja ds la bdd
			if ((marqueDAO.getIdentifiant(obj.getNom())).getId() == 0) {
				ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
						"INSERT INTO Marque (nom) VALUES ('"+obj.getNom()+"')");
				resultat2.close();
				this.conn.commit();
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;	
	}

	/**
	 * M�thode de suppression d'un objet Marque de la table Marque
	 */
	public boolean delete(Marque obj) {
		DAO_abstr<Marque> marqueDAO = new MarqueDAO(HsqldbConnection.getInstance());
		if(obj != null) {
			System.out.println("id de objet suppr : "+(marqueDAO.getIdentifiant(obj.getNom())).getId());//--Controle
			try {
				this.conn.setAutoCommit(false);
				
				//--On controle la marque supprimee 
				Marque marqSuppr = marqueDAO.getIdentifiant(obj.getNom());
				System.out.println("Marque supprimee : "+marqSuppr.toString());//--Controle
				
				if (marqSuppr != null) {
					//--On supprime
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
							"DELETE FROM Marque WHERE id = "+(marqueDAO.getIdentifiant(obj.getNom())).getId());
					resultat.close();
					this.conn.commit();
					return true;
				}
			}catch (SQLException e) {
				e.printStackTrace();				
			}
		}
		return false;
	}

	/**
	 * M�thode de modification d'un objet Marque dans la table Marque
	 */
	public boolean update(Marque obj) {
			try {
				this.conn.setAutoCommit(false);
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
						"UPDATE Marque SET nom = '"+(obj.getNom())+"' WHERE id = "+obj.getId());
				resultat.close();
				this.conn.commit();
				return true;
			}catch (SQLException e) {
			e.printStackTrace();
			}				
		return false;
	}

	/**
	 * M�thode de recherche d'un objet Marque dans la table Marque
	 */
	public Marque find(int id) {
		Marque marque = new Marque();
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Marque WHERE id = "+id);
			if (resultat.first())
				marque = new Marque(id, resultat.getString("nom"));
			resultat.close();
			this.conn.commit();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return marque;
	}
	/**
	 * Methode retournant l'Id de l'objet Marque 
	 */
	public Marque getIdentifiant(String pNom) {
		Marque marque = new Marque();
		int idMarque = 0;
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id FROM Marque WHERE nom = '"+pNom+"'");
			if(resultat.first()) {
				idMarque = (Integer)resultat.getInt("Id");
				marque = new Marque(idMarque, pNom);
			}
			resultat.close();	
			this.conn.commit();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return marque;
	}

}

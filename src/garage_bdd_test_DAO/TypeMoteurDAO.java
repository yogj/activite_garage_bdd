package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.ocr.sql.HsqldbConnection;
import voiture.moteur.TypeMoteur;
/**
 * Classe d�finissant l'objet TypeMoteurDAO
 * @author nicolas
 *
 */
public class TypeMoteurDAO extends DAO_abstr<TypeMoteur> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public TypeMoteurDAO(Connection pConn) {
		super(pConn);
	}

	/**
	 * M�thode de cr�ation d'un objet TypeMoteur dans la table type_moteur 
	 */
	public boolean create(TypeMoteur obj) {
		DAO_abstr<TypeMoteur> tmDAO = new TypeMoteurDAO(HsqldbConnection.getInstance());
			try {
				this.conn.setAutoCommit(false);
				//On verifie que l'objet n'existe pas deja ds la bdd
				if ((tmDAO.getIdentifiant(obj.getNom())).getId() == 0) {
					//--Creation du type de moteur
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"INSERT INTO Type_moteur(descritpion) VALUES ('"+obj.getNom()+"')");
					resultat.close();
					return true;
				}
				this.conn.commit();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		return false;
	}

	/**
	 * M�thode de suppression d'un objet TypeMoteur de la table type_moteur 
	 */
	public boolean delete(TypeMoteur obj) {
		DAO_abstr<TypeMoteur> tmDAO = new TypeMoteurDAO(HsqldbConnection.getInstance());
			try {
				this.conn.setAutoCommit(false);
				//--On controle le type de moteur supprime 
				TypeMoteur tmSuppr = tmDAO.getIdentifiant(obj.getNom());
				System.out.println("Type de Moteur supprime : "+tmSuppr.toString());//--Controle
				
				if (tmSuppr != null) {
					//--On supprime
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
							"DELETE FROM Type_moteur WHERE id = "+(tmDAO.getIdentifiant(obj.getNom())).getId());
					resultat.close();
					this.conn.commit();
					return true;
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
		return false;
	}

	/**
	 * M�thode de modification d'un objet TypeMoteur dans la table type_moteur 
	 */
	public boolean update(TypeMoteur obj) {
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"UPDATE Type_moteur SET description = '"+obj.getNom()+
					"' WHERE id = "+obj.getId());
			resultat.close();
			this.conn.commit();
			return true;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * M�thode de recherche d'un objet TypeMoteur dans la table type_moteur 
	 */
	public TypeMoteur find(int id) {
		TypeMoteur type = new TypeMoteur();
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Type_moteur WHERE id = "+id);
			if (resultat.first())
				type = new TypeMoteur(id, resultat.getString("description"));
			resultat.close();
			this.conn.commit();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return type;
	}
	/**
	 * Methode retournant l'Id de l'objet TypeMoteur 
	 */
	public TypeMoteur getIdentifiant(String pDescription) {
		TypeMoteur tm = new TypeMoteur();
		int idTM = 0;
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id FROM Type_moteur WHERE DESCRIPTION = '"+pDescription+"'");
			if(resultat.first()) {
				idTM = (Integer)resultat.getInt("Id");
				tm = new TypeMoteur(idTM, pDescription);
			}
			resultat.close();	
			this.conn.commit();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return tm;
	}

}

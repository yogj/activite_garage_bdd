package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.ocr.sql.HsqldbConnection;
import voiture.option.Option;
/**
 * Classe d�finissant l'objet OptionDAO
 * @author nicolas
 *
 */
public class OptionDAO extends DAO_abstr<Option> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public OptionDAO(Connection pConn) {
		super(pConn);
	}
	
	/**
	 *M�thode de cr�ation d'un objet Option dans la table option 
	 */
	public boolean create(Option obj) {
		DAO_abstr<Option> optionDAO = new OptionDAO(HsqldbConnection.getInstance());
			try {
				this.conn.setAutoCommit(false);
				//On verifie que l'objet n'existe pas deja ds la bdd
				if ((optionDAO.getIdentifiant(obj.getNom())).getId() == 0) {
					//--Creation de l'option
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
						"INSERT INTO Option(description, prix) VALUES ('"+obj.getNom()+"',"+obj.getPrix()+")");
					resultat.close();
					this.conn.commit();
					return true;
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
		return false;
	}

	/**
	 * M�thode de suppression d'un objet Option de la table option 
	 */
	public boolean delete(Option obj) {
		DAO_abstr<Option> optionDAO = new OptionDAO(HsqldbConnection.getInstance());
		if(obj != null) {
			System.out.println("id de objet suppr : "+(optionDAO.getIdentifiant(obj.getNom())).getId());//--Controle
			try {
				this.conn.setAutoCommit(false);
				//--On controle l'option supprimee
				Option optSuppr = optionDAO.getIdentifiant(obj.getNom());
				System.out.println("Option supprimee : "+optSuppr.toString());//--Controle
				
				if (optSuppr != null) {
					//--On supprime
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
							"DELETE FROM Option WHERE id = "+(optionDAO.getIdentifiant(obj.getNom())).getId());
					resultat.close();
					this.conn.commit();
					return true;
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * M�thode de modification d'un objet Option dans la table option 
	 */
	public boolean update(Option obj) {
		try {
			this.conn.setAutoCommit(false);	
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"UPDATE Option SET description = '"+obj.getNom()+"', prix = "+obj.getPrix()+
					" WHERE id = "+obj.getId());
			resultat.close();
			this.conn.commit();
			return true;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * M�thode de recherche d'un objet Option dans la table option 
	 */
	public Option find(int id) {
		Option option = new Option();
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Option WHERE id = "+id);
			if (resultat.first())
				option = new Option(id, resultat.getString("description"), resultat.getDouble("prix"));
			resultat.close();
			this.conn.commit();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return option;
	}
	/**
	 * Methode retournant l'Id de l'objet Option 
	 */
	public Option getIdentifiant(String pDescription) {
		Option opt = new Option();
		int idOpt = 0;
		double prixOpt = 0d;
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id, prix FROM Option WHERE DESCRIPTION = '"+pDescription+"'");
			if(resultat.first()) {
				idOpt = (Integer)resultat.getInt("Id");
				prixOpt = (Double)resultat.getDouble("PRIX");
				opt = new Option(idOpt, pDescription, prixOpt);
				System.out.println("OptionDAO_getIdentifiant = "+opt.toString());
			}
			resultat.close();
			this.conn.commit();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return opt;
	}
}

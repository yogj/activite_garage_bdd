package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import fr.ocr.sql.HsqldbConnection;
import voiture.Marque;
import voiture.Vehicule;
import voiture.moteur.Moteur;
import voiture.option.Option;


/**
 * Classe d�finissant l'objet VehiculeDAO
 * @author nicolas
 *
 */
public class VehiculeDAO extends DAO_abstr<Vehicule> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public VehiculeDAO(Connection pConn) {
		super(pConn);
	}

	/**
	 * M�thode de cr�ation d'un objet Vehicule dans les tables vehicule et vehicule_option
	 */
	public boolean create(Vehicule obj) {
		int ID = 0;
		try {
			// On d�marre notre transction
			this.conn.setAutoCommit(false);
						 
			//--On creee le vehicule ds la table vehicule
			 ResultSet resultat1 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					 "INSERT INTO Vehicule (marque, moteur, prix, nom)"
					 + " VALUES ("+obj.getMarque().getId()+","+obj.getMoteur().getId()+", "+obj.getPrix()+",'"+obj.getNom()+"')");
			 resultat1.close();
			 
			// Nous allons r�cup�rer le prochain ID
			ResultSet nextID = this.conn.prepareStatement("CALL NEXT VALUE FOR seq_vehicule_id").executeQuery();
			if (nextID.next()) {
				ID = nextID.getInt(1)-1;
				System.out.println("id du vehicule cree : "+ID);//Controle
				obj.setId(ID);
			}
			//nextID.close();
		 
			 //--Si le vehicule a des options
			 if(obj.getOptions()!= null) {
				 //--Pour chaque option du v�hicule, on l'inscrit ds la table vehicule_option
				 for (Option opt : obj.getOptions()) {
					 System.out.println("DEBUG vehiculeDAO #2 : INSERT INTO Vehicule_option (id_vehicule, id_option) VALUES("+obj.getId() +","+opt.getId()+")");//--Controle
					 ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
						 "INSERT INTO Vehicule_option (id_vehicule, id_option) VALUES("+obj.getId() +","+opt.getId()+")");
					 resultat2.close();
				 }
			 }
			this.conn.commit();
			 return true;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 *M�thode de suppression d'un objet Vehicule dans les tables vehicule et vehicule_option 
	 */
	public boolean delete(Vehicule obj) {
		DAO_abstr<Vehicule> vehicDAO = new VehiculeDAO(HsqldbConnection.getInstance());
			try {
				this.conn.setAutoCommit(false);
				//--On controle le vehicule supprime et ses options
				Vehicule vehSuppr = vehicDAO.getIdentifiant(obj.getNom());
				System.out.println("vehicule supprime : "+vehSuppr.toString());//--Controle
				
				if(vehSuppr != null) {
					//--On supprime les options du vehicules dans la table vehicule_option
					ResultSet resultat1 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
							"DELETE FROM Vehicule_option WHERE id_vehicule = "+(vehicDAO.getIdentifiant(obj.getNom())).getId());
					//--On supprime le vehicule ds la table vehicule
					ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
							"DELETE FROM Vehicule WHERE id = "+(vehicDAO.getIdentifiant(obj.getNom())).getId());
					
					resultat1.close();
					resultat2.close();
					this.conn.commit();
					return true;
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
		return false;
	}

	/**
	 * M�thode de modification d'un objet Vehicule dans les tables vehicule et vehicule_option
	 */
	public boolean update(Vehicule obj) {
			try {
				this.conn.setAutoCommit(false);
				//--On modif le vehicule ds la table vehicule
				ResultSet resultat1 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
						"UPDATE Vehicule SET nom = '"+obj.getNom()+"', marque = '"+obj.getMarque()+"', moteur = '"+obj.getMoteur()+"',"
								+ "prix = '"+obj.getPrix()+"' WHERE id = "+obj.getId());
				//--On modif les options du vehicule ds la table vehicule_option
				for (Option opt : obj.getOptions()) {
				ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
						"UPDATE Vehicule_option SET id_option = '"+(opt.getId()+"'WHERE id_vehicule = "+obj.getId()));
				resultat2.close();
				}
				resultat1.close();
				this.conn.commit();
				return true;
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
	}

	/**
	 * M�thode de recherche d'un objet Vehicule dans la tables vehicule et de ses options dans la table vehicule_option
	 */
	public Vehicule find(int id) {
		Vehicule voiture = new Vehicule();
		Marque marque = new Marque();
		Moteur moteur = new Moteur();
		Option opt = new Option();
		ArrayList<Option> lopt = new ArrayList<Option>();
		DAO_abstr<Marque> marqueDAO = new MarqueDAO(HsqldbConnection.getInstance());
		DAO_abstr<Moteur> motDAO = new MoteurDAO(HsqldbConnection.getInstance());
		DAO_abstr<Option> optDAO = new OptionDAO(HsqldbConnection.getInstance());
		
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat1 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Vehicule WHERE id = "+id);//INNER JOIN vehicule_option ON id_vehicule = id AND id = "+id+" LEFT JOIN option ON id_option = option.id");
			if(resultat1.first()) {
				//--On recup la marque
				marque = marqueDAO.find(resultat1.getInt("MARQUE"));
				
				//--On recupere le moteur
				moteur = motDAO.find(resultat1.getInt("MOTEUR"));
			}
			//--On cree l'objet Vehicule avec une liste d'option vide
			voiture = new Vehicule(id, resultat1.getString("nom"), marque, moteur, lopt, resultat1.getDouble("prix"));
			resultat1.close();
				
			//--On recup les options via une requete
			ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT (id_option) FROM Vehicule_option WHERE id_vehicule = "+voiture.getId());
			if(resultat2.first()){
				resultat2.beforeFirst();
				while(resultat2.next()) {
					opt = optDAO.find(resultat2.getInt("id_option"));
					lopt.add(opt);
				}
			}
			resultat2.close();
			this.conn.commit();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return voiture;
	}
	/**
	 * Methode retournant l'Id de l'objet Vehicule 
	 */
	public Vehicule getIdentifiant(String pNom) {
		Vehicule vehic = new Vehicule();
		Marque marque = new Marque();
		Moteur moteur = new Moteur();
		Option opt = new Option();
		ArrayList<Option> lopt = new ArrayList<Option>();
		int idVoit = 0;
		double prix = 0;
		DAO_abstr<Marque> marqueDAO = new MarqueDAO(HsqldbConnection.getInstance());
		DAO_abstr<Moteur> motDAO = new MoteurDAO(HsqldbConnection.getInstance());
		DAO_abstr<Option> optDAO = new OptionDAO(HsqldbConnection.getInstance());
		
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat1 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Vehicule WHERE nom = '"+pNom+"'");
			if(resultat1.first()) {
				idVoit = (Integer)resultat1.getInt("Id");
				//--On recup la marque
				marque = marqueDAO.find(resultat1.getInt("MARQUE"));
				
				//--On recupere le moteur
				moteur = motDAO.find(resultat1.getInt("MOTEUR"));
				
				//--On recupere le prix
				prix = (Double)resultat1.getDouble("PRIX");
				}
			resultat1.close();	
			
			//--On recup les options dans la table de jointure via une requete				
			ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id_option FROM Vehicule_option WHERE id_vehicule = "+idVoit);
			if(resultat2.first()) {
				while(resultat2.next()) {
					opt = optDAO.find(resultat2.getInt("id_option"));
					lopt.add(opt);		
				}
			}
			resultat2.close();
			
			//--on cree le vehicule avec les attributs recup�rer
			vehic = new Vehicule(idVoit, pNom, marque, moteur, lopt, prix);
			this.conn.commit();
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return vehic;
	}
	

}

package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.ocr.sql.HsqldbConnection;
import voiture.moteur.Moteur;
import voiture.moteur.TypeMoteur;

/**
 * Classe d�finissant l'objet MoteurDAO
 * @author nicolas
 *
 */
public class MoteurDAO extends DAO_abstr<Moteur>{
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public MoteurDAO(Connection pConn) {
		super(pConn);
	}

	/**
	 * M�thode de cr�ation d'un objet Moteur dans la table Moteur
	 */
	public boolean create(Moteur obj) {
		DAO_abstr<Moteur> moteurDAO = new MoteurDAO(HsqldbConnection.getInstance());
			try {
				this.conn.setAutoCommit(false);
				//On verifie que l'objet n'existe pas deja ds la bdd
				if ((moteurDAO.getIdentifiant(obj.getCylindre())).getId() == 0) {
					ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
							"INSERT INTO Moteur (cylindre, moteur, prix) "
							+ "VALUES ("+obj.getCylindre()+",'"+obj.getType()+"'," +obj.getPrix()+")");
					resultat2.close();
					this.conn.commit();
					return true;
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
		return false;
	}

	/**
	 * M�thode de suppression d'un objet Moteur de la table Moteur
	 */
	public boolean delete(Moteur obj) {
		DAO_abstr<Moteur> moteurDAO = new MoteurDAO(HsqldbConnection.getInstance());
		if(obj != null) {
			System.out.println("id de objet suppr : "+(moteurDAO.getIdentifiant(obj.getCylindre())).getId());//--Controle
			try {
				this.conn.setAutoCommit(false);
				//--On controle le moteur supprime
				Moteur motSuppr = moteurDAO.getIdentifiant(obj.getCylindre());
				System.out.println("Option supprimee : "+motSuppr.toString());//--Controle
				
				if (motSuppr != null) {
					//--On supprime
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
							"DELETE FROM Moteur WHERE id = "+(moteurDAO.getIdentifiant(obj.getCylindre())).getId());
					resultat.close();
					this.conn.commit();
					return true;
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * M�thode de modifcation d'un objet Moteur dans la table Moteur
	 */
	public boolean update(Moteur obj) {
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"UPDATE Moteur SET cylindre = '"+(obj.getCylindre())+"', moteur = '"+obj.getType()+"', prix = '"+obj.getPrix()+
					"' WHERE id = "+obj.getId());
			resultat.close();
			this.conn.commit();
			return true;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * M�thode de recherche d'un objet Moteur dans la table Moteur
	 */
	public Moteur find(int id) {
		Moteur moteur  = new Moteur();
		DAO_abstr<TypeMoteur> tmDAO = new TypeMoteurDAO(HsqldbConnection.getInstance());
		TypeMoteur tm = new TypeMoteur();
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Moteur WHERE id = "+id);
			if (resultat.first()) {
				tm = tmDAO.find(resultat.getInt("MOTEUR")); 
				moteur = new Moteur(id, tm, resultat.getString("cylindre"), resultat.getDouble("prix"));
			}
			resultat.close();	
			this.conn.commit();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return moteur;
	}
	/**
	 * Methode retournant l'Id de l'objet Moteur
	 */
	public Moteur getIdentifiant(String pCylindre) {
		Moteur moteur = new Moteur();
		int idMoteur = 0;
		double prixMot = 0;
		DAO_abstr<TypeMoteur> tmDAO = new TypeMoteurDAO(HsqldbConnection.getInstance());
		TypeMoteur tm = new TypeMoteur();
		try {
			this.conn.setAutoCommit(false);
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id, prix FROM Moteur WHERE cylindre = '"+pCylindre+"'");
			if(resultat.first()) {
				idMoteur = (Integer)resultat.getInt("Id");
				tm = tmDAO.find(resultat.getInt("Id"));
				prixMot = (Double)resultat.getDouble("Prix");
				moteur = new Moteur(idMoteur, tm, pCylindre, prixMot);
				System.out.println("MoteurDAO_getidentifiant = "+moteur.toString());
			}
			resultat.close();	
			this.conn.commit();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return moteur;
	}

}
